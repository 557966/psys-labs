/**
 * Test for functions in core.c
 *
 * We don't need to test anything here really.
 */
#include <assert.h>

/* Include the module under testing */
#include "core.c"

/******************************************************/

static void test__calculate_minus(void)
{
	assert(5 == minus(8,3));
	assert(-2 == minus(-3,-1));
}

/**
 * Main entry for the test.
 */
int main(int argc, char **argv)
{
    test__calculate_minus();
	return 0;
}
