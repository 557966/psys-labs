/**
 * Implements the core of the algorithm.
 *
 * @file core.c
 */

#include "core.h"


int minus(int x, int y)
{
	return x-y;
}
