#include <iostream>
#include <iomanip>
#include <sys/times.h>
#include <cmath>
#include <omp.h>

#include <unistd.h>

/*
 * compiling with make flags results in failures
 * OMP_NUM_THREADS=4 ./pip
 * run rime is 2-8 sec
 */

double f_square(double y) {
    return(4.0 / (1.0 + y*y));
}

double f_cube(double y) {
    return(4.0 / (1.0 + y*y*y));
}

int main(int argc, char** argv)
{
    int numSteps;
    double step, pi, x;
    double PI25DT = 3.141592653589793238462643;
    double sum = 0;
    double (*f)(double);

    if(argc == 3) {
        numSteps = atoi(argv[1]);
        int func = atoi(argv[2]);
        if(func==0) {f = &f_square;}
        if(func==1) {f = &f_cube;}
        printf("numSteps=%d and f=%d (f=0 is square, f=1 is cube)\n", numSteps, func);
    }
    else {
        numSteps=500000000;                     /* default # of rectangles 500kk */
        f = &f_cube;
        printf("using default args with numSteps=500kk and f=cube\n");
    }

#pragma omp parallel
    {
#pragma omp master
        {
            int cntThreads=omp_get_num_threads();
            std::cout<<"OpenMP. number of threads = "<<cntThreads<<std::endl;
        }
    }

    clock_t clockStart, clockStop;
    tms tmsStart, tmsStop;
    step = 1./(double)numSteps;
    clockStart = times(&tmsStart);

#pragma omp parallel for private (x), reduction (+:sum)
    for (int i=0; i<numSteps; i++)
    {
        x = (i + .5)*step;
        sum = sum + f(x);
    }
    pi = sum*step;

    clockStop = times(&tmsStop);
    std::cout << "The value of PI is " << pi << " Error is " << fabs(pi - PI25DT) << std::endl;
    std::cout << "The time to calculate PI was " ;
    double secs= (clockStop - clockStart)/static_cast<double>(sysconf(_SC_CLK_TCK));
    std::cout << secs << " seconds\n" << std::endl;
    return 0;
}